# arm-fusion

## contributing
to contribute in writing a feature (tasks 1-9):
1. Creat a branche named `feature/${feature_name}`, ex: `git checkout -b feature/sections-table`. If two or more are working on the same task branches are named `feature/${feature_name}-${author}`. Note: make sure you're in the master branche
1. Commit your changes, eg: `git commit -am 'add function print_sec_table(..)'`
1. Push to the branch `git push origin feature/sections-table`



## Fixing compiling
Type the below commands
```
sudo apt install gcc-multilib-arm-linux-gnueabi
./configure
make
```
