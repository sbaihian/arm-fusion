#ifndef __SECTION_CONTENT_H__
#define __SECTION_CONTENT_H__

#include<elf.h> 
#include<stdio.h>
#include<stdlib.h>
#include "util.h"
#include<string.h>

/*
   Print_Sec
   description : Affiche le contenu d'une section x recuperer depuis la ligne de cmd
   parametres : file    : le fichier
   				num_sec : numero de la section a afficher
   				header  : l'entet du fichier
				sh_tab  : la table les headers de section 
   valeur de retour : Aucune (Affichage uniquement)
   effets de bord : Aucun
*/

void Print_Sec(FILE *file,int num_sec, Elf32_Ehdr *header, Elf32_Shdr *sh_tab);


/*
   get_name_sec
   description : permet de recuperer le nom la section x  
   parametres : file      : le fichier
   				num_sec   : numero de la section a trouver
   				shstrndx  : indice d'entrée a la  table  des  chaînes  de  noms  des sections 
				sh_tab    : la table les headers de section 
   valeur de retour : Une chaine de caractere (Nom de section ou un message d'erreur)
   effets de bord : Aucun
*/
char *get_name_sec(FILE *file,int num_sec,Elf32_Half shstrndx, Elf32_Shdr *sh_tab);

#endif