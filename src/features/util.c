/*
ELF Loader - chargeur/implanteur d'exécutables au format ELF à but pédagogique
Copyright (C) 2012 Guillaume Huard
Ce programme est libre, vous pouvez le redistribuer et/ou le modifier selon les
termes de la Licence Publique Générale GNU publiée par la Free Software
Foundation (version 2 ou bien toute autre version ultérieure choisie par vous).

Ce programme est distribué car potentiellement utile, mais SANS AUCUNE
GARANTIE, ni explicite ni implicite, y compris les garanties de
commercialisation ou d'adaptation dans un but spécifique. Reportez-vous à la
Licence Publique Générale GNU pour plus de détails.

Vous devez avoir reçu une copie de la Licence Publique Générale GNU en même
temps que ce programme ; si ce n'est pas le cas, écrivez à la Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307,
États-Unis.

Contact: Guillaume.Huard@imag.fr
         ENSIMAG - Laboratoire LIG
         51 avenue Jean Kuntzmann
         38330 Montbonnot Saint-Martin
*/
#include "util.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <unistd.h>

int elf32_is_big = 0;

int elf_is_big_endian() {
    return elf32_is_big;
}

int is_big_endian() {
    static uint32_t one = 1;
    return ((* (uint8_t *) &one) == 0);
}

// size_t __wrap_fread(int fildes, void *buf, size_t nbyte)
// {
//         printf("called********\n");
//         size_t r = __real_fread(fildes, buf, nbyte);

//         // if((is_big_endian() != elf32_is_big))
//         // {
//         //         if(nbyte == sizeof(uint32_t))
//         //         {
//         //                 uint32_t tmp1 = *((uint32_t*) buf);
//         //                 uint32_t *tmp2 = (uint32_t*) buf;
//         //                 *tmp2 = reverse_4(tmp1);
//         //         }
//         //         else if(nbyte == sizeof(uint16_t))
//         //         {
//         //                 uint16_t tmp1 = *((uint16_t*) buf);
//         //                 uint16_t *tmp2 = (uint16_t*) buf;
//         //                 *tmp2 = reverse_2(tmp1);
//         //         }
//         // }

//         return r;
// }