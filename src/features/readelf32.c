#include "readelf32.h"


// global variables used for handling input

// pointer to the elf_file name
char *file_name;

// vars for args parsing/handling

bool opt_error = false;

bool opt_usage = false;
bool opt_file_header = false;
bool opt_sections = false;
bool opt_sec_content = false;
bool opt_symbols = false;
bool opt_relocs = false;

// vars for option args

int *opt_sec_content_argv = NULL;
int opt_sec_content_argc = 0;



void parse_args(int argc, char ** argv) {

    // options string
    const char* const short_opts = "hHSx:sr";

    // available long_options
    const struct option long_opts[] = {
        {"help",            no_argument,        NULL, 'H'},
        {"file-header",     no_argument,        NULL, 'h'},
        {"sections",        no_argument,        NULL, 'S'},
        {"hex-dump", required_argument,  NULL, 'x'},
        {"syms",         no_argument,        NULL, 's'},
        {"relocs",          no_argument,        NULL, 'r'},
        {NULL, 0, NULL, 0}
    };

    int next_opt;


    // parsing input

    do {

        next_opt = getopt_long(argc, argv, short_opts, long_opts, NULL);

        // if(*argv[optind-1] != '-') {
        //     file_name = argv[optind-1];
        //     files_counter++;
        // }

        switch (next_opt) {

            case 'h':
                opt_file_header = true;

                break;
            
            case 'H':
                opt_usage = true;

                break;

            case 'S':
                // TODO print sections table
                opt_sections = true;

                break;

            case 'x':
                // TODO: print contenct of section
                opt_sec_content = true;
                opt_sec_content_argv = (int *) realloc(opt_sec_content_argv, sizeof(int));
                *(opt_sec_content_argv + opt_sec_content_argc) = atoi(optarg);
                opt_sec_content_argc++;
                
                break;

            case 's':
                // TODO: print symbols table
                opt_symbols = true;
                
                break;

            case 'r':
                opt_relocs = true;

                break;

            case -1:
                if(argc == 1)
                    print_usage();
                break;

            case '?':
                // checking if an invalid option is passed

                if(*argv[optind-1] == '-') {
                    opt_error = true;
                    printf("Invalid option <%s>, check -H (--help) for valide options\n", argv[optind-1]); 
                }        

                break;
        
            default:
                break;
        }

    } while (next_opt != -1);

}

void process_file(FILE *elf_file) {

    Elf32_Ehdr *elf_header = get_elf_header(elf_file);

    Elf32_Shdr *sec_table = get_sec_table(elf_file, elf_header);

    // checking options


    if(opt_file_header) {
        printf("\n\n");
        print_elf_header(elf_file);
    }

    
    if(opt_sections) {
        printf("\n\n");
        print_sec_table(elf_file, sec_table, elf_header);
    }


    if(opt_relocs) {
        printf("\n\n");
        print_reloc_table(elf_file, elf_header, sec_table);
    }
    

    if(opt_sec_content) {
        printf("\n\n");

        // print section content of sections (if more than a section is entred)
        for(int i = 0; i < opt_sec_content_argc; i++) {
            Print_Sec(elf_file, opt_sec_content_argv[i],elf_header, sec_table);
        }
    }


    if(opt_symbols) {
        printf("\n\n");
        print_symtable(elf_file, elf_header, sec_table);
    }


    if(opt_usage) {
        printf("\n\n");
        print_usage();
    }
}

void print_usage() {
    printf("Usage: readelf32 <option(s)> elf-file(s)\n");

    printf(" %s\n %s\n",
            "Display information about the contents of ELF format files",
            "Options are:");
    
    printf("  %-23s %s\n", "-H --help", "Display this usage informations");
    printf("  %-23s %s\n", "-h --file-header", "Display the ELF file header");
    printf("  %-23s %s\n", "-S --section-headers", "Display the sections' header");
    printf("  %-23s %s\n", "-s --syms", "Display the symbol table");
    printf("  %-23s %s\n", "-r --relocs", "Display this usage informations");
    printf("  %-23s %s\n", "-x --hex-dump", "Dump the contents of section");
    // TODO: complet usage options
}


int main(int argc, char *argv[]) {

    FILE *elf_file = NULL;



    // parsing args
    parse_args(argc, argv);


    for(; optind < argc; optind++){

        file_name = argv[optind];
        if(access(file_name, F_OK) == -1) {
            printf("file %s not exists\n", file_name);
            continue;
        }

        printf("File: %s\n", file_name);

        // opening the file
        elf_file = fopen(file_name, "rb");

        // process file (printing)
        process_file(elf_file);

        // closing the file
        fclose(elf_file);
    } 

    
    return 0;
}
