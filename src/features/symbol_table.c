#include "symbol_table.h"

void get_name_symbole(FILE * file, int num_sec, Elf32_Ehdr * e, Elf32_Shdr * sh_tab, Elf32_Word stname) {

  // trouver la strtab
  int indice = 0;
  for (int i = 0; i < reverse_4(e -> e_shnum); ++i) {
    if (reverse_4(sh_tab[i].sh_type) == SHT_STRTAB && strcmp(".strtab", get_name_sec(file, i, reverse_2(e -> e_shstrndx), sh_tab)) == 0) {
      indice = i;
      break;
    }
  }

  Elf32_Word shsize = reverse_4(sh_tab[indice].sh_size);
  Elf32_Off shoffset = reverse_4(sh_tab[indice].sh_offset);
  char * chaine_noms = malloc(shsize);

  if (fseek(file, shoffset, SEEK_SET) == -1) {
    printf("POINTEUR non designé\n");
  } else {

    if (fread(chaine_noms, shsize, 1, file) == 0) {
      printf("Lecture Echouée\n");
    } else {
      //  Elf32_Word shname = reverse_4(sh_tab[num_sec].sh_name);
      chaine_noms += stname;
      printf("%s", chaine_noms);
    }
  }
}

// char * get_name_sec(FILE * file, int num_sec, Elf32_Half shstrndx, Elf32_Shdr * sh_tab) {

//   Elf32_Word shsize = reverse_4(sh_tab[shstrndx].sh_size);
//   Elf32_Off shoffset = reverse_4(sh_tab[shstrndx].sh_offset);
//   char * chaine_noms = malloc(shsize);

//   if (fseek(file, shoffset, SEEK_SET) == -1) {
//     return "POINTEUR non designé\n";
//   } else {

//     if (fread(chaine_noms, shsize, 1, file) == 0) {
//       return "Lecture Echouée\n";
//     } else {
//       Elf32_Word shname = reverse_4(sh_tab[num_sec].sh_name);
//       chaine_noms += shname;
//       return chaine_noms;
//     }
//   }
// }

char * getBindingVal(int val) {
  switch (val) {
  case (0):
    return "LOCAL";
    break;
  case (1):
    return "GLOBAL";
    break;
  case (2):
    return "WEAK";
    break;
  case (13):
    return "LOPROC";
    break;
  case (15):
    return "HIPROC";
    break;
  default:
    return ("UNKNOWN");
  }
}

//Renvoie le type du symbole (NOTYPE, SECTION, FUNC, etc.)
char * getTypeSymbol(int val) {
  switch (val) {
  case (0):
    return "NOTYPE";
    break;
  case (1):
    return "OBJECT";
    break;
  case (2):
    return "FUNC";
    break;
  case (3):
    return "SECTION";
    break;
  case (4):
    return "FILE";
    break;
  case (13):
    return "LOPROC";
    break;
  case (15):
    return "HIPROC";
    break;
  default:
    exit(1);
  }
}

char * getVisibility(int val) {
  switch (val) {
  case STV_DEFAULT:
    return "DEFAULT";
    break;
  case STV_INTERNAL:
    return "INTERNAL";
    break;
  case STV_HIDDEN:
    return "HIDDEN";
    break;
  case STV_PROTECTED:
    return "PROTECTED";
    break;
  default:
    return "UNKNOWN";
  }
}

// void main(int argc, char * argv[]) {
//   Elf32_Ehdr * e;
//   int fd;

//   // vérification des arguments
//   if (argc != 2) {
//     fprintf(stderr, "usage: %s [NOM DU FICHIER]\n", argv[0]);
//     exit(1);
//   }

//   // ouverture du fichier
//   fd = open(argv[1], O_RDONLY);

//   // vérification ouverture
//   if (fd < 0) {
//     fprintf(stderr, "ERREUR: ouverture de %s impossible\n", argv[1]);
//     perror("open");
//     exit(1);
//   }

//   // Get length of file
//   struct stat st;
//   fstat(fd, & st);
//   int fileLen = st.st_size;

//   FILE * f = fopen(argv[1], "r+");

//   // chargement 
//   e = (Elf32_Ehdr * ) mmap(0, fileLen, PROT_READ, MAP_PRIVATE, fd, 0);

//   // récupération de la table des symboles 
//   int i;

//   print_symtable(f, e, get_sh_tab(f, e));

// }

Elf32_Shdr * get_sh_tab(FILE * f, Elf32_Ehdr * e) {
  int shnum = reverse_2(e -> e_shnum);
  int shsize = reverse_2(e -> e_shentsize);
  int offset = reverse_4(e -> e_shoff);
  Elf32_Shdr * sh_tab = malloc(shnum * shsize);
  fseek(f, offset, SEEK_SET);
  fread(sh_tab, shnum * shsize, 1, f);
  return sh_tab;
}

void print_symtable(FILE * f, Elf32_Ehdr * e, Elf32_Shdr * sh_tab) {
//   int tmp;
  int ne = is_big_endian();
//   int shentsize = (ne == 0) ? reverse_4(e -> e_shentsize) : e -> e_shentsize;
  int shnum = (ne == 0) ? reverse_4(e -> e_shnum) : e -> e_shnum;
//   int shoff = (ne == 0) ? reverse_4(e -> e_shoff) : e -> e_shoff;
//   int symtab_off = 0, strtab_off = 0, symtab_size = 0;
int symtab_off = 0;
//   int i, strtab_index, lus, index;
int index;
//   Elf32_Off symtable;
  // récupération de la table
  for (int i = 0; i < shnum; i++) {
    if (reverse_4(sh_tab[i].sh_type) == SHT_SYMTAB) {
      index = i;
      symtab_off = reverse_4(sh_tab[i].sh_offset);

      break;
    }
  }
  int nb_symboles = reverse_4(sh_tab[index].sh_size) / sizeof(Elf32_Sym);
  Elf32_Sym * symb = malloc(sizeof(Elf32_Sym) * nb_symboles);
  fseek(f, symtab_off, SEEK_SET);
  if (fread(symb, sizeof(Elf32_Sym) * nb_symboles, 1, f) == -1) {
    printf("Lecture Echouée");
  }


  printf("La table de symboles << .symtab >> contient %d entrées\n", nb_symboles);
  printf("   Num:    Valeur Tail Type    Lien   Vis      Ndx Nom\n");

  for (int j = 0; j < nb_symboles; j++) {
    printf("    %2d: ", j);
    printf("%08x  ", reverse_4(symb[j].st_value));
    printf("%4d ", reverse_4(symb[j].st_size));
    printf("%-7s ", getTypeSymbol(ELF32_ST_TYPE(symb[j].st_info)));
    printf("%-7s", getBindingVal(ELF32_ST_BIND(symb[j].st_info)));
    printf("%s ", getVisibility(ELF32_ST_VISIBILITY(symb[j].st_other)));

    if (reverse_2(symb[j].st_shndx) == 0)
      printf(" UND ");
    else
      printf("%4d ", reverse_2(symb[j].st_shndx));

    get_name_symbole(f, index, e, sh_tab, reverse_4(symb[j].st_name));

    puts("");

  }

}