#ifndef _READELF32_H_
#define _READELF32_H_

#include <stdio.h>
#include <unistd.h>
#include <getopt.h>

#include "print_header.h"
#include "section_table.h"
#include "section_content.h"
#include "reloc_table.h"
#include "symbol_table.h"

 /* defining bool type */
typedef enum { false, true } bool;


/* function to parse input */
void parse_args(int argc, char ** argv);

/* function to process file */
void process_file(FILE *elf_file);

/* print --help usage infos */
void print_usage();




#endif