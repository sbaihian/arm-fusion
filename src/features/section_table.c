#include "section_table.h"




// Elf32_Ehdr *get_elf_header(FILE *elf_file) {

//     assert(elf_file != NULL);
    
//     // switch to Elf32_Ehdr for x86 architecture.
//     Elf32_Ehdr *header = malloc(sizeof(Elf32_Ehdr));
//     fseek(elf_file, 0, SEEK_SET);    
//     fread(header, 1, sizeof(Elf32_Ehdr), elf_file);
    
//     // check so its really an elf file
//     if(memcmp(header->e_ident, ELFMAG, SELFMAG))
//         return NULL;

//     // printf("Valide ELF file read.\n");

//     // TODO: handling big/little endians
    
//     return header;
    
//     }


Elf32_Shdr *get_sec_table(FILE *elf_file, Elf32_Ehdr *elf_header) {

    // check whether the passed elf_file is pointing to a file
    assert(elf_file != NULL);

    // allocate tentries
    Elf32_Shdr *sec_table = malloc(reverse_2(elf_header->e_shnum) * reverse_2(elf_header->e_shentsize));

    // seeking start of file
    fseek(elf_file, reverse_4(elf_header->e_shoff), SEEK_SET);

    // reading table of entries
    fread(sec_table, reverse_2(elf_header->e_shentsize), reverse_2(elf_header->e_shnum), elf_file);

    return sec_table;
}

char *get_sec_name_tab(FILE *elf_file, Elf32_Ehdr *elf_header, Elf32_Shdr *sec_table) {

    Elf32_Half str_tab_id = reverse_2(elf_header->e_shstrndx);
    Elf32_Word str_tab_size = reverse_4(sec_table[str_tab_id].sh_size);
    Elf32_Off str_tab_off = reverse_4(sec_table[str_tab_id].sh_offset);
    char *str_tab = malloc(str_tab_size);

    fseek(elf_file, str_tab_off, SEEK_SET);
    fread(str_tab, str_tab_size, 1, elf_file);

    return str_tab;
}


char *get_sec_type(Elf32_Word value) {
        switch (value)
        {
        case SHT_NULL:
                return "NULL";
                break;
        case SHT_PROGBITS:
                return "PROGBITS";
                break;
        case SHT_SYMTAB:
                return "SYMTAB";
                break;
        case SHT_STRTAB:
                return "STRTAB";
                break;
        case SHT_RELA:
                return "RELA";
                break;
        case SHT_HASH:
                return "HASH";
                break;
        case SHT_DYNAMIC:
                return "DYNAMIC";
                break;
        case SHT_NOTE:
                return "NOTE";
                break;
        case SHT_NOBITS:
                return "NOBITS";
                break;
        case SHT_REL:
                return "REL";
                break;
        case SHT_SHLIB:
                return "SHLIB";
                break;
        case SHT_DYNSYM:
                return "DYNSYM";
                break;
        case SHT_INIT_ARRAY:
                return "INIT_ARRAY";
                break;
        case SHT_FINI_ARRAY:
                return "FINI_ARRAY";
                break;
        case SHT_PREINIT_ARRAY:
                return "PREINIT_ARRAY";
                break;
        case SHT_GROUP:
                return "GROUP";
                break;
        case SHT_SYMTAB_SHNDX:
                return "SYMTAB_SHNDX";
                break;
        case SHT_NUM:
                return "NUM";
                break;
        case SHT_LOOS:
                return "LOOS";
                break;
        case SHT_GNU_ATTRIBUTES:
                return "GNU_ATTRIBUTES ";
                break;
        case SHT_GNU_HASH:
                return "GNU_HASH";
                break;
        case SHT_GNU_LIBLIST:
                return "GNU_LIBLIST";
                break;
        case SHT_CHECKSUM:
                return "CHECKSUM";
                break;
        case SHT_SUNW_move:
                return "SUNW_move";
                break;
        case SHT_SUNW_COMDAT:
                return "SUNW_COMDAT";
                break;
        case SHT_SUNW_syminfo:
                return "SUNW_syminfo";
                break;
        case SHT_GNU_verdef:
                return "GNU_verdef";
                break;
        case SHT_GNU_verneed:
                return "GNU_verneed";
                break;
        case SHT_GNU_versym:
                return "GNU_versym";
                break;
        case SHT_LOPROC:
                return "LOPROC";
                break;
        case SHT_HIPROC:
                return "HIPROC";
                break;
        case SHT_LOUSER:
                return "LOUSER";
                break;
        case SHT_HIUSER:
                return "HUISER";
                break;
        case SHT_ARM_EXIDX:
                return "ARM_EXIDX";
                break;
        case SHT_ARM_PREEMPTMAP:
                return "ARM_PREEMPTMAP";
                break;
        case SHT_ARM_ATTRIBUTES:
                return "ARM_ATTRIBUTES ";
                break;
        default:
                return "NULL";
                break;
        }
}

char *get_sec_flags(Elf32_Word value) {
    char *flgs = (char*) malloc(sizeof(char)*3);

    if (value & SHF_WRITE)
            strcat(flgs, "W");

    if (value & SHF_ALLOC)
            strcat(flgs, "A");

    if (value & SHF_EXECINSTR)
            strcat(flgs, "X");

    if (value & SHF_MERGE)
            strcat(flgs, "M");

    if (value & SHF_STRINGS)
            strcat(flgs, "S");

    if (value & SHF_INFO_LINK)
            strcat(flgs, "I");

    if (value & SHF_LINK_ORDER)
            strcat(flgs, "L");

    if (value & SHF_OS_NONCONFORMING)
            strcat(flgs, "x");

    if (value & SHF_GROUP)
            strcat(flgs, "G");

    if (value & SHF_TLS)
            strcat(flgs, "T");

    if (value & SHF_MASKOS)
            strcat(flgs, "o");

    if (value & SHF_MASKPROC)
            strcat(flgs, "p");

    if (value & SHF_ORDERED)
            strcat(flgs, "O");

    if (value & SHF_EXCLUDE)
            strcat(flgs, "E");
    
    return flgs;
}

void print_sec_table(FILE *elf_file, Elf32_Shdr *sec_table, Elf32_Ehdr *elf_header) {
    
    char * flgs;

    // reading string name table of sections
    char *str_tab = get_sec_name_tab(elf_file, elf_header, sec_table);

    // number of sections
    Elf32_Half nb_sec = reverse_2(elf_header->e_shnum);

    // printing data

    printf("There are %d section headers, starting at offset 0x%X:\n\n", nb_sec, reverse_4(elf_header->e_shoff));
    printf("Section Headers:\n");
    printf("  [Nr] %-18s %-18s %-8s %-6s %-6s %s %s %s %s %s\n", "Name", "Type", "Addr", "Off", "Size", "ES", "Flg", "Lk", "Inf", "Al");


    for(int i = 0; i < nb_sec; i++) {
            flgs = get_sec_flags(reverse_4(sec_table[i].sh_flags));
        printf("  [%2d] %-18s %-18s %08d %06x %06x %02d %3s %2d %3d %2d\n",
            i,
            str_tab+reverse_4(sec_table[i].sh_name),
            get_sec_type(reverse_4(sec_table[i].sh_type)),
            reverse_4(sec_table[i].sh_addr),
            reverse_4(sec_table[i].sh_offset),
            reverse_4(sec_table[i].sh_size),
            reverse_4(sec_table[i].sh_entsize),
            flgs,
            reverse_4(sec_table[i].sh_link),
            reverse_4(sec_table[i].sh_info),
            reverse_4(sec_table[i].sh_addralign));

            free(flgs);

    }
    
    printf("Key to Flags:\n");
    printf("  W (write), A (alloc), X (execute), M (merge), S (strings), I (info),\n");
    printf("  L (link order), O (extra OS processing required), G (group), T (TLS),\n");
    printf("C (compressed), x (unknown), o (OS specific), E (exclude),\n");
    printf("  y (purecode), p (processor specific)\n");

}


