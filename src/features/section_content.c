#include "section_content.h"

char *get_name_sec(FILE * file, int num_sec, Elf32_Half shstrndx, Elf32_Shdr * sh_tab) {

    Elf32_Word shsize = reverse_4(sh_tab[shstrndx].sh_size);
    Elf32_Off shoffset = reverse_4(sh_tab[shstrndx].sh_offset);
    char * chaine_noms = malloc(shsize);

    if (fseek(file, shoffset, SEEK_SET) == -1) {
        return "POINTEUR non designé\n";
    } else {

        if (fread(chaine_noms, shsize, 1, file) == 0) {
            return "Lecture Echouée\n";
        } else {
            Elf32_Word shname = reverse_4(sh_tab[num_sec].sh_name);
            chaine_noms += shname;
            return chaine_noms;
        }
    }
}



void Print_Sec(FILE * file, int num_sec, Elf32_Ehdr * header, Elf32_Shdr * sh_tab) {

    if (num_sec >= reverse_2(header->e_shnum)) {
        printf("AVERTISSEMENT: La section %d n'a pas été vidangée parce qu'inexistante !\n",num_sec );
        return;
    }

    Elf32_Half shstrndx = reverse_2(header -> e_shstrndx);
    Elf32_Word shtype = reverse_4(sh_tab[num_sec].sh_type);
    Elf32_Word shsize = reverse_4(sh_tab[num_sec].sh_size);
    Elf32_Off shoffset = reverse_4(sh_tab[num_sec].sh_offset);

    if (shstrndx == SHN_UNDEF) {
        printf("le fichier ne comporte pas de table des chaînes de noms des sections\n");

        return;
    } else {
        // NOM SECTION 
        if (shstrndx >= SHN_LORESERVE) {
            shstrndx = sh_tab[0].sh_link;
        }

        if (shtype == SHT_NOBITS || shtype == 0 || shsize == 0) {
            printf("La section « ");
            printf("%s",get_name_sec(file, num_sec, shstrndx, sh_tab));
            printf(" » n'a pas de données à vidanger.\n");
            return;
        } else {
            printf("Vidange hexadécimale de la section «");
            printf("%s",get_name_sec(file, num_sec, shstrndx, sh_tab));
            printf("» :\n");
        }
        /*****************************************/
        // CONTENU SECTION
        uint32_t curr_addr = 0;
        int shsize2 = shsize;
        fseek(file, shoffset, SEEK_SET);
        int car_manq=16-shsize2 % 16;
        int alire=16;
        int i = 0;
        while(i <= (shsize / 16) && shsize2 >0)
        {
            char * buffer = malloc(32);
            uint32_t * ptr = (uint32_t * ) buffer;
            
            if (shsize2 != 0) {
                printf("  0x%08x ", curr_addr);
                curr_addr += 16;
            }

            
            if (shsize2 >= 16)
            {   for (int i = 0; i < 4; ++i)
                {
                    uint32_t data;
                    fread( &data, 4, 1, file);
                    * ptr = data;
                    ptr++;
                    printf("%08x ", reverse_4(data));
                    shsize2 -= 4;
                }
                for (int i = 0; i < alire; ++i) {
                
                    if (buffer[i] < 32 || buffer[i] > 127)
                        printf(".");
                    else
                        printf("%c", buffer[i]);        
                }
            
                free(buffer);
                puts("");
                
            }else{
                
                uint8_t * reste_data = malloc(shsize2);
                for (int i = 1; i <= shsize2; ++i) {
                    reste_data[i] = fgetc(file);
                    printf("%02x", reste_data[i]);
                    if (i % 4 == 0)
                    {
                        printf(" ");
                    }
                }
                alire=16 - car_manq;
                for (int i = 0; i < car_manq; ++i)
                {
                    printf("  ");
                }
                float f=(float)car_manq / 4;
                if(f > (int)f )
                    f=(int) f+1;
                for (int i = 0; i < (int) f; ++i)
                {
                   printf(" ");
                }
                for (int i = 0; i < alire; ++i) {
                
                    if (reste_data[i] < 32 || reste_data[i] > 127)
                        printf(".");
                    else
                        printf("%c", reste_data[i]);        
                }
                free(buffer);
                puts("");
        
                }

            ++i;
        }
    }
}
