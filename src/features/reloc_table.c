#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "section_content.h"
#include "util.h"
#include<elf.h>


void afficher_type(Elf32_Word type){
    switch (type) {
    case R_ARM_NONE:
        printf("R_ARM_NONE");
        break;
    case R_ARM_PC24:
        printf("R_ARM_PC24");
        break;
    case R_ARM_ABS32:
        printf("R_ARM_ABS32");
        break;
    case R_ARM_REL32:
        printf("R_ARM_REL32");
        break;
    case R_ARM_PC13:
        printf("R_ARM_PC13");
        break;
    case R_ARM_ABS16:
        printf("R_ARM_ABS16");
        break;
    case R_ARM_ABS12:
        printf("R_ARM_ABS12");
        break;
    case R_ARM_THM_ABS5:
        printf("R_ARM_THM_ABS5");
        break;
    case R_ARM_ABS8:
        printf("R_ARM_ABS8");
        break;
    case R_ARM_SBREL32:
        printf("R_ARM_SBREL32");
        break;
    case R_ARM_THM_PC22:
        printf("R_ARM_THM_PC22");
        break;
    case R_ARM_THM_PC8:
        printf("R_ARM_THM_PC8");
        break;
    case R_ARM_AMP_VCALL9:
        printf("R_ARM_AMP_VCALL9");
        break;
    case R_ARM_TLS_DESC:
        printf("R_ARM_TLS_DESC");
        break;
    case R_ARM_THM_SWI8:
        printf("R_ARM_THM_SWI8");
        break;
    case R_ARM_XPC25:
        printf("R_ARM_XPC25");
        break;
    case R_ARM_THM_XPC22:
        printf("R_ARM_THM_XPC22");
        break;
    case R_ARM_TLS_DTPMOD32:
        printf("R_ARM_TLS_DTPMOD32");
        break;
    case R_ARM_TLS_DTPOFF32:
        printf("R_ARM_TLS_DTPOFF32");
        break;
    case R_ARM_TLS_TPOFF32:
        printf("R_ARM_TLS_TPOFF32");
        break;
    case R_ARM_COPY:
        printf("R_ARM_COPY");
        break;
    case R_ARM_GLOB_DAT:
        printf("R_ARM_GLOB_DAT");
        break;
    case R_ARM_JUMP_SLOT:
        printf("R_ARM_JUMP_SLOT");
        break;
    case R_ARM_RELATIVE:
        printf("R_ARM_RELATIVE");
        break;
    case R_ARM_GOTOFF:
        printf("R_ARM_GOTOFF");
        break;
    case R_ARM_GOTPC:
        printf("R_ARM_GOTPC");
        break;
    case R_ARM_GOT32:
        printf("R_ARM_GOT32");
        break;
    case R_ARM_PLT32:
        printf("R_ARM_PLT32");
        break;
    case R_ARM_CALL:
        printf("R_ARM_CALL");
        break;
    case R_ARM_JUMP24:
        printf("R_ARM_JUMP24");
        break;
    case R_ARM_ALU_PCREL_7_0:
        printf("R_ARM_ALU_PCREL_7_0");
        break;
    case R_ARM_ALU_PCREL_15_8:
        printf("R_ARM_ALU_PCREL_15_8");
        break;
    case R_ARM_ALU_PCREL_23_15:
        printf("R_ARM_ALU_PCREL_23_15");
        break;
    case R_ARM_LDR_SBREL_11_0:
        printf("R_ARM_LDR_SBREL_11_0");
        break;
    case R_ARM_ALU_SBREL_19_12:
        printf("R_ARM_ALU_SBREL_19_12");
        break;
    case R_ARM_ALU_SBREL_27_20:
        printf("R_ARM_ALU_SBREL_27_20");
        break;
    case R_ARM_V4BX:
        printf("R_ARM_V4BX");
        break;
    case R_ARM_TLS_GOTDESC:
        printf("R_ARM_TLS_GOTDESC");
        break;
    case R_ARM_TLS_CALL:
        printf("R_ARM_TLS_CALL");
        break;
    case R_ARM_TLS_DESCSEQ:
        printf("R_ARM_TLS_DESCSEQ");
        break;
    case R_ARM_THM_TLS_CALL:
        printf("R_ARM_THM_TLS_CALL");
        break;
    case R_ARM_GNU_VTENTRY:
        printf("R_ARM_GNU_VTENTRY");
        break;
    case R_ARM_GNU_VTINHERIT:
        printf("R_ARM_GNU_VTINHERIT");
        break;
    case R_ARM_THM_PC11:
        printf("R_ARM_THM_PC11");
        break;
    case R_ARM_THM_PC9:
        printf("R_ARM_THM_PC9");
        break;
    case R_ARM_TLS_GD32:
        printf("R_ARM_TLS_GD32");
        break;
    case R_ARM_TLS_LDM32:
        printf("R_ARM_TLS_LDM32");
        break;
    case R_ARM_TLS_LDO32:
        printf("R_ARM_TLS_LDO32");
        break;
    case R_ARM_TLS_IE32:
        printf("R_ARM_TLS_IE32");
        break;
    case R_ARM_TLS_LE32:
        printf("R_ARM_TLS_LE32");
        break;
    case R_ARM_THM_TLS_DESCSEQ:
        printf("R_ARM_THM_TLS_DESCSEQ");
        break;
    case R_ARM_IRELATIVE:
        printf("R_ARM_IRELATIVE");
        break;
    case R_ARM_RXPC25:
        printf("R_ARM_RXPC25");
        break;
    case R_ARM_RSBREL32:
        printf("R_ARM_RSBREL32");
        break;
    case R_ARM_THM_RPC22:
        printf("R_ARM_THM_RPC22");
        break;
    case R_ARM_RREL32:
        printf("R_ARM_RREL32");
        break;
    case R_ARM_RABS22:
        printf("R_ARM_RABS22");
        break;
    case R_ARM_RPC24:
        printf("R_ARM_RPC24");
        break;
    case R_ARM_RBASE:
        printf("R_ARM_RBASE");
        break;
    case R_ARM_NUM:
        printf("R_ARM_NUM");
        break;
    default:
        printf("UNKNOWN");
    }
}



void print_reloc_table (FILE * file,Elf32_Ehdr *header, Elf32_Shdr *sh_tab) {

	Elf32_Half eshnum=reverse_2(header->e_shnum);
	//Elf32_Word   sh_type;
	// parcours de la table de sections
	Elf32_Rel* rel_content;

	for (int i = 0; i < eshnum; ++i)
	{	
		
		if (reverse_4(sh_tab[i].sh_type) == SHT_REL ||reverse_4(sh_tab[i].sh_type) == SHT_RELA )
		{
			int nb_ent=reverse_4(sh_tab[i].sh_size)/sizeof(Elf32_Rel);
			
			printf("Section de réadressage '");
			printf("%s",get_name_sec(file,i,reverse_2(header->e_shstrndx),sh_tab));			
			printf("' à l'adresse de décalage 0x%x contient %d entrée:\n",reverse_4(sh_tab[i].sh_offset),nb_ent);
			printf(" Cible     Type       Indice reloc \n" );
			
			rel_content=malloc(sizeof(Elf32_Rel)*nb_ent);
			
			fseek(file,reverse_4(sh_tab[i].sh_offset),SEEK_SET);	
		
			if(fread(rel_content,sizeof(Elf32_Rel)*nb_ent,1,file))
			{
				for (int i = 0; i < nb_ent ; ++i)
				{
					//printf("%s\n",getSymNamesTable(file,header,i ,sh_tab) );
					printf("%08x  ",reverse_4(rel_content[i].r_offset));//, sym_tab[ELF_R_SYM(rel_content[i].r_info)].st_name	
					afficher_type(ELF32_R_TYPE(reverse_4(rel_content[i].r_info)));
					//uint8_ c=ELF32_R_INFO(ELF32_R_SYM(reverse_4(rel_content[i].r_info)),ELF32_R_TYPE(reverse_4(rel_content[i].r_info)));
					 	
					printf(" %s",get_name_sec(file,ELF32_R_SYM(reverse_4(rel_content[i].r_info)),reverse_2(header->e_shstrndx) ,sh_tab) );
					//printf("%d\n", reverse_4(rel_content[i].r_));
					puts("");
				}		
			}
			else
				printf("entrees de la de la table de réimplantation non lue\n");	
			free(rel_content);
		}
	}
}
