#ifndef __SECTION_TABLE_H__
#define __SECTION_TABLE_H__

#include <elf.h>

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

// #include "print_header.h"
#include "util.h"


/*
   get_sec_table
   description : Function to read the ELF section table
   parametres : elf_file    : the ELF file
   				elf_header : ELF file header
   returned valur : none
   bord effects : none
*/
Elf32_Shdr *get_sec_table(FILE *elf_file, Elf32_Ehdr *elf_header);


/*
   get_sec_table
   description : function to get section name string
   parametres : elf_file    : the ELF file
   				elf_header : ELF file header
                sec_table : section header table of ELF file
   returned valur : string, section name
   bord effects : none
*/
char *get_sec_name_tab(FILE *elf_file, Elf32_Ehdr *elf_header, Elf32_Shdr *sec_table);

/*
   get_sec_flags
   description : function to get section name string
   parametres : value    : value if section->flgs
   returned valur : string, flage name
   bord effects : none
*/
char *get_sec_flags(Elf32_Word value);

/*
   get_sec_type
   description : function to get section name string
   parametres : value    : value of section->type
   returned valur : string, type name
   bord effects : none
*/
char *get_sec_type(Elf32_Word value);

/*
   get_sec_table
   description : function to print ELF section table
   parametres : elf_file    : the ELF file
                sec_table   : section headers tables of ELF file
   				elf_header : ELF file header
   returned valur : none
   bord effects : none
*/
void print_sec_table(FILE *elf_file, Elf32_Shdr *sec_table, Elf32_Ehdr *elf_header);




#endif