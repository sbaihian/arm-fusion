#ifndef _PRINT_HEADER_H_
#define _PRINT_HEADER_H_

#include <elf.h>
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "util.h"

/* Function to read  ELF header */
Elf32_Ehdr *get_elf_header(FILE *elf_file);


/*
 print_type
     Descr : Affiche le Type d'un fichier ELF
     Param :
         - type: le type concerné
 */
void print_type(Elf32_Half type);


/*
 get_data
     Descr  : Affiche l'endianness des data du fichier ELF
     Param  :
         - x: l'int qui determine l'endianness (1 little endian, 2 big endian)
	 return :
    		  chaine de caractere (type de l'endianness) 
 */
char* get_data(int x);


/*
 print_class
     Descr : Affiche la classe du fichier ELF (32/64 bits)
     Param :
         - header : entete du fichier ELF

 */
void print_class(Elf32_Ehdr header);


/*
 print_version
     Descr : Affiche la version de l’en-tête du fichier ELF
     Param :
         - header : entete du fichier ELF
 */
void print_version(Elf32_Ehdr header);


/*
 get_abi
     Descr  : recupere l'OS/ABI du fichier ELF
     Param  :
         - x: l'int qui determine l'OS/ABI (e_ident[EI_OSABI])
     return :
     		  chaine de caractere correspondante     

 */
char* get_abi(int x);


/*
 machineCible
     Descr  :  affiche l'architecture requise pour un fichier ELF
     Param  :
         - a:  valeur de e_machine du header  
 */
void machineCible(Elf32_Half a);


/*
 print_flags
     Descr :  affiche le début des en-têtes de section en Bytes
     Param :
         - header : header du fichier ELF  
 */
void print_flags(Elf32_Ehdr header);


/*
 read_elf_header
     Descr  :  Affiche à l'ecran l'entete du fichier passé en parametre
     Param  :
         - file : Fichier dans lequel sera lu l'entete 
     Return :
        Header du fichier donné 

 */
Elf32_Ehdr print_elf_header(FILE* file);



#endif