#include "print_header.h"
#include <stdio.h>
#include <stdlib.h>
#include <elf.h>
#include <string.h>

#include "print_header.h"
#include "util.h"

//[Phase 1:Etape 1 //

Elf32_Ehdr *get_elf_header(FILE *elf_file) {
    assert(elf_file != NULL);
    
    Elf32_Ehdr *header = malloc(sizeof(Elf32_Ehdr));
    fseek(elf_file, 0, SEEK_SET);    
    fread(header, 1, sizeof(Elf32_Ehdr), elf_file);
    
    // check so its really an elf file
    if(memcmp(header->e_ident, ELFMAG, SELFMAG))
        return NULL;

    // printf("Valide ELF file read.\n");

    // TODO: handling big/little endians (wrap the read function)
    if(header->e_ident[EI_DATA] == ELFDATA2LSB)
		elf32_is_big = 0;
	else if(header->e_ident[EI_DATA] == ELFDATA2MSB)
		elf32_is_big = 1;
    
    return header;
}



    



//Affiche la classe du fichier ELF (ELF32/64)
void print_class(Elf32_Ehdr header)
{
    /* Class */
    printf("  Class:                             %c%c%c",
           header.e_ident[EI_MAG1],
           header.e_ident[EI_MAG2],
           header.e_ident[EI_MAG3]);

    int class=header.e_ident[EI_CLASS];
    switch(class) {
        case ELFCLASSNONE:
            printf("INVALID");
            break;
        case ELFCLASS32:
            printf("32");
            break;
        case ELFCLASS64:
            printf("64");
            break;
        default:
            printf("UNKNOWN");
            break;
    }
}

//Affiche l'endianness des data du fichier ELF
char* get_data(int x){
	char* data="";
	switch(x) {
	   case 0  :
	      data= "Aucun";
	      break;
	   case 1  :
	      data= "little endian";
	      break;
	   case 2  :
	      data= "big endian";
	      break;
		default:
			  printf("data non connue");
	}
	return data;
}

//Affiche la version de l’en-tête du fichier ELF
void print_version(Elf32_Ehdr header)
{
    printf("\n  Version:                           ");
    printf("%d", header.e_ident[EI_VERSION]);

    switch(header.e_ident[EI_VERSION])
    {
        case EV_CURRENT:
            printf(" (current)");
            break;
        case EV_NONE:
            printf(" (invalid)");
            break;
        default:
            printf(" Unknown");
            break;
    }
}


//Affiche le Type d'un fichier ELF
void print_type(Elf32_Half type)
{
    printf("\n  Type:                              ");

    switch(reverse_2(type))
    {
        case ET_NONE:
            printf("No file type");
        case ET_REL:
            printf("Relocatable file");
            break;
        case ET_EXEC:
            printf("Executable file");
            break;
        case ET_DYN:
            printf("Shared object file");
            break;
        case ET_CORE:
            printf("Core file");
            break;
        case ET_NUM:
            printf("Number of defined types");
            break;
        case ET_LOOS:
            printf("OS-specific range start");
            break;
        case ET_HIOS:
            printf("OS-specific range end");
            break;
        case ET_LOPROC:
            printf("Processor-specific range start");
            break;
        case ET_HIPROC:
            printf("Processor-specific range end");
            break;
        default:
            printf("Unknown Type");
    }
}


//recupere l'OS/ABI du fichier ELF
char* get_abi(int x){
	char* abi;
	switch(reverse_4(x)) {
	   case 0  :
	      abi= "UNIX - System V";
	      break;
	   case 1  :
	      abi= "HP-UX";
	      break;
	   case 2  :
	      abi= "NetBSD";
	      break;
	   case 3  :
	      abi= "LINUX";
	      break;
	   case 6  :
	      abi= "Sun Solaris";
	      break;
	   case 7  :
	      abi= "IBM AIX";
	      break;

	   case 8  :
	      abi= "SGI Irix";
	      break;
	   case 9  :
	      abi= "FreeBSD";
	      break;
	   case 10  :
	      abi= "Compaq TRU64";
	      break;
	   case 11  :
	      abi= "Novell Modesto";
	      break;
	   case 12  :
	      abi= "OpenBSD";
	      break;
	   case 64  :
	      abi= "ARM EABI";
	      break;
	   case 97  :
	      abi= "ARM";
	      break;
	   case 255  :
	      abi= "Standalone";
	      break;
	   default :
	   	abi= "Non reconnue !";
	}
	return abi;
}

//affiche l'architecture requise pour un fichier ELF
void machineCible(Elf32_Half a){
	char* machine;

	switch (reverse_2(a)){
	  case 0:
		machine= "No machine";
		break;
	  case 1:
		machine= "AT&T WE 32100";
		break;
	  case 2:
		machine= "SPARC";
		break;
	  case 3:
		machine= "Intel 80386";
		break;
	  case 4:
		machine= "Motorola 68000";
		break;
	  case 5:
		machine= "Motorola 88000";
		break;
	  case 7:
		machine= "Intel 80860";
		break;
	  case 8:
		machine= "MIPS RS3000";
		break;
	  case 19:
		machine= "Intel i960";
		break;
	  case 20:
		machine= "PowerPC";
		break;
	  case 40:
		machine= "ARM";
		break;
	  case 50:
		machine= "Intel IA64";
		break;
	  case 62:
		machine= "x64";
		break;
	}
	printf("%s",machine);
}

#define pref printf(", ")

//affiche le début des en-têtes de section en Bytes

void print_flags(Elf32_Ehdr header){

  int32_t e_flags= reverse_4(header.e_flags);

    printf("  Flags:                             0x%x", e_flags);
    pref;

    if(e_flags & EF_ARM_RELEXEC) {

        printf("Relocatable Executable");
				pref;
    }
    if(e_flags & EF_ARM_HASENTRY ) {
        printf("Has entry point");
				pref;
    }
    if(e_flags & EF_ARM_INTERWORK ) {
        printf("Inter work");
				pref;
     }
    if(e_flags & EF_ARM_APCS_26 ) {
        printf("APCS_26");
				pref;
    }
    if(e_flags & EF_ARM_APCS_FLOAT ) {
        printf("APCS_FLOAT");
				pref;
    }
    if(e_flags & EF_ARM_PIC ) {
        printf("PIC");
				pref;
    }
    if( e_flags & EF_ARM_ALIGN8	 ) {
        printf("ALIGN8");
				pref;
    }
    if(e_flags & EF_ARM_NEW_ABI ) {
        printf("NEW_ABI");
				pref;
    }
    if(e_flags & EF_ARM_OLD_ABI ) {
        printf("OLD_ABI");
				pref;
    }
    if(e_flags & EF_ARM_SOFT_FLOAT ) {
        printf("soft-float ABI");
				pref;
    }
    if(e_flags & EF_ARM_VFP_FLOAT ) {
        printf("VFP_FLOAT");
				pref;
    }
    if(e_flags & EF_ARM_MAVERICK_FLOAT ) {
        printf("MAVERICK_FLOAT");
				pref;
    }

    /* EABI VERSION FLAG */
    if(e_flags & EF_ARM_EABIMASK) {
        printf("Version");
    }

    if( e_flags &  EF_ARM_EABI_UNKNOWN) {
        printf(" Unknown");
				pref;
    }
    else {
        printf("%d ", e_flags >> (6*4));
    }
    if( e_flags &  EF_ARM_EABIMASK) {
        printf("EABI");
    }
}

//Affiche à l'ecran l'entete du fichier passé en parametre
Elf32_Ehdr print_elf_header(FILE* file) {

  Elf32_Ehdr header;
  rewind(file);

			    // read the header
	if (fread(&header, 1, sizeof(header), file)!=0){

				printf("ELF Header:\n");
				printf("  Magic:   ");

				for(int i=0;i<16;i++)
				{ printf("%02x ",header.e_ident[i]);}
				printf("\n");
				print_class(header);
				printf("\n  Data:                              %s",get_data(header.e_ident[EI_DATA]));
				print_version(header);
				printf("\n  OS/ABI:                            %s",get_abi(header.e_ident[EI_OSABI]));
				printf("\n  ABI Version:                       %d",reverse_4(header.e_ident[EI_ABIVERSION]));
				print_type(header.e_type);
				printf("\n  Machine:                           ");machineCible(header.e_machine);
				printf("\n  Version:                           0x%1x",reverse_2(header.e_ident[EI_VERSION]));
				printf("\n  Entry point address:               0x%1x",reverse_4(header.e_entry));
				printf("\n  Start of program headers:          %u (bytes into file)",reverse_4(header.e_phoff));
				printf("\n  Start of section headers:          %u (bytes into file)\n",reverse_4(header.e_shoff));
				print_flags(header);
				printf("\n  Size of this header:               %u (bytes)",reverse_2(header.e_ehsize));
				printf("\n  Size of program headers:           %u (bytes)",reverse_2(header.e_phentsize));
				printf("\n  Number of program headers:         %u",reverse_2(header.e_phnum));
				printf("\n  Size of section headers:           %u (bytes)",reverse_2(header.e_shentsize));
				printf("\n  Number of section headers:         %u",reverse_2(header.e_shnum));
				printf("\n  Section header string table index: %u",reverse_2(header.e_shstrndx));
				printf("\n");
				return header;

	  }
		else exit(1);

}
