#ifndef _PRINT_SYMTABLE_H_
#define _PRINT_SYMTABLE_H_

#include <elf.h>
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "util.h"
#include "section_content.h"

void get_name_symbole(FILE * file, int num_sec, Elf32_Ehdr * e, Elf32_Shdr * sh_tab, Elf32_Word stname);
// char * get_name_sec(FILE * file, int num_sec, Elf32_Half shstrndx, Elf32_Shdr * sh_tab);
void print_symtable(FILE * f, Elf32_Ehdr * e, Elf32_Shdr * sh_tab);
Elf32_Shdr * get_sh_tab(FILE * f, Elf32_Ehdr * e);
char * getBindingVal(int val);
char * getTypeSymbol(int val);
char * getVisibility(int val);

#endif